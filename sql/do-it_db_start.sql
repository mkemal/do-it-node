-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 10. Mrz 2022 um 13:18
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `do-it`
--
DROP DATABASE IF EXISTS `do-it`;
CREATE DATABASE IF NOT EXISTS `do-it` DEFAULT CHARACTER SET latin1 COLLATE latin1_german1_ci;
USE `do-it`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `password` varchar(400) COLLATE latin1_german1_ci NOT NULL,
  `desc` varchar(400) COLLATE latin1_german1_ci NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_name_uidx` (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `account`
--

INSERT INTO `account` (`id`, `name`, `password`, `desc`, `createdAt`, `updatedAt`) VALUES
(1, 'mktester', 'test1234', '', '2022-03-10 13:01:57', '2022-03-10 13:01:57'),
(2, 'agtester', 'test1234', '', '2022-03-10 13:01:57', '2022-03-10 13:01:57');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `todo`
--

DROP TABLE IF EXISTS `todo`;
CREATE TABLE IF NOT EXISTS `todo` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `title` varchar(4000) COLLATE latin1_german1_ci NOT NULL,
  `due_date` datetime NOT NULL,
  `est_time` int(11) NOT NULL,
  `done` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `todo`
--

INSERT INTO `todo` (`id`, `account_id`, `title`, `due_date`, `est_time`, `done`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'schlafen', '2022-03-11', 8, 0, '2022-03-10 00:00:00', '2022-03-10 13:02:45'),
(2, 2, 'arbeiten', '2022-03-10', 8, 0, '2022-03-10 00:00:00', '2022-03-10 13:02:45');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
