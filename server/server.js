import express from "express";
import db from "./config/database.js";
import routes from "./routes/index.js";
import cors from "cors";
const app = express();
const IP = "127.0.0.1";
const PORT = 3002;
try {
	await db.authenticate();
	console.log("Database connected...");
} catch (error) {
	console.error("Connection error:", error);
}

app.use((_req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.use(cors());
app.use(express.json());
app.use("/", routes);

app.listen(PORT, IP, () => {
	console.log(`http://${IP}:${PORT}/`);
});
