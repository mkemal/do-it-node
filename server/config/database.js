import { Sequelize } from "sequelize";

export default new Sequelize("do-it", "doit", "test1234", {
	host: "localhost",
	dialect: "mysql",
	define: {
		timestamps: true,
		freezeTableName: true
	}
});

