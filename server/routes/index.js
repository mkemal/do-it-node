import express from "express";

import {
	getAllAccounts,
	getAccountByName,
	createAccount,
	getAccountById,
	updateAccount,
	deleteAccount,
	getAllToDos,
	getToDoByTitle,
	getToDoById,
	createToDo,
	updateToDo,
	deleteToDo,
	getAllTodosByAccountId,
	deleteToDoByIdList,
	deleteToDoByAccountId,
	checkPassword

} from "../controllers/Controller.js";

const router = express.Router();

router.get("/api/accounts", getAllAccounts); // OK
router.get("/api/account-by-name/:name", getAccountByName); // OK
router.get("/api/account/:id", getAccountById); // OK 
router.post("/api/account/create", createAccount); // OK
router.patch("/api/account/update/:id", updateAccount); // OK
router.delete("/api/account/delete/:id", deleteAccount); // OK
router.post("/api/todos", getAllToDos); // OK
router.post("/api/todos-by-accountId", getAllTodosByAccountId);
router.get("/api/todo-by-title/:title", getToDoByTitle);
router.get("/api/todo/:id", getToDoById);
router.post("/api/todo/create", createToDo);
router.patch("/api/todo/update/:id", updateToDo);
router.delete("/api/todo/delete/:id", deleteToDo);
router.delete("/api/todo/deleteToDoByAccountId/:account_id", deleteToDoByAccountId);
router.post("/api/todo/deleteToDoByIdList", deleteToDoByIdList);

router.post("/api/password/check", checkPassword);

export default router;