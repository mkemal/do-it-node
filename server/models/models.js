import { Sequelize, DataTypes } from "sequelize";
import bcrypt from "bcrypt";

import db from "../config/database.js";

// Model definieren
// Klasse dient als Grundlage für Erzeugung/Arbeit mit Datenbank-Tabelle
export const Account = db.define("account", {
	id: {
		type: DataTypes.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
		type: DataTypes.STRING,
		allowNull: false
	},
	password: {
		type: DataTypes.TEXT,
		allowNull: false
	},
	desc: {
		type: DataTypes.TEXT
	},
	createdAt: {
		type: DataTypes.DATE, defaultValue: Sequelize.NOW
	},
	updatedAt: {
		type: DataTypes.DATE, defaultValue: Sequelize.NOW
	}
},
{
	hooks: {
		beforeCreate: (user) => {
			if (user.password) {
				if (user.password) {
					const salt = bcrypt.genSaltSync(10, "a");
					user.password = bcrypt.hashSync(user.password, salt);
				}
			}
		}
	}
});
export const ToDo = db.define("todo", {
	id: {
		type: DataTypes.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true
	},
	account_id: {
		type: DataTypes.INTEGER,
		allowNull: false
	},
	title: {
		type: DataTypes.TEXT,
		allowNull: false
	},
	due_date: {
		type: DataTypes.DATE
	},
	est_time: {
		type: DataTypes.INTEGER
	},
	done: {
		type: DataTypes.BOOLEAN
	},
	createdAt: {
		type: DataTypes.DATE, defaultValue: Sequelize.NOW
	},
	updatedAt: {
		type: DataTypes.DATE, defaultValue: Sequelize.NOW
	}
});




