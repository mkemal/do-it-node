import { Account, ToDo } from "../models/models.js";
import bcrypt from "bcrypt";

const badResponse = {
	"success": false,
	data: []
};

// GET Request
export const getAllToDos = async (req, res) => {
	try {
		// Will escape title and validate DESC against a list of valid direction parameters
		const orderby = req.body.orderby || [['id', 'DESC']] ;
		const todoList = await ToDo.findAll({order: orderby});
		if (todoList?.length) {
			res.json({
				"success": true,
				"data": todoList
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error);
		res.json(badResponse);
	}
};
// GET Request
export const getAllTodosByAccountId = async (req, res) => {
	try {
		// Will escape title and validate DESC against a list of valid direction parameters
		const orderby = req.body.orderby || [['id', 'DESC']] ;
		const accountId = req.body.accountId;
		//console.log(accountId)
		if (!accountId) {
			console.error("bad id");
			res.send(badResponse);
		}
		const todos = await ToDo.findAll({
			where: {
				account_id: accountId
			},
			order: orderby
		});
		if (todos?.length) {
			//console.log(`todos: ${todos}, id: ${accountId}`);
			res.json({
				"success": true,
				data: todos
			});
			return;
		}else{
			res.json(badResponse);
			return
		}
	
	} catch (error) {
		console.error(error);
	}
	res.json(badResponse);
	return
};
// GET Request
export const getToDoById = async (req, res) => {
	try {
		const updateId = req.params.id;
		const todo = await findTodoById(updateId);
		if (todo) {
			res.json({
				"success": true,
				"data": todo
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};
// GET Request
export const getToDoByTitle = async (req, res) => {
	try {
		const searchTitle = req.params.title;
		const todo = await findTodoByTitle(searchTitle);
		if (todo) {
			res.json({
				"success": true,
				"data": todo
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error);
		res.json(badResponse);
	}
};
// POST Request
export const createToDo = async (req, res) => {
	try {
		const title = req.body.title;
		const accountId = req.body.account_id;
		const existingTodo = await findTodoByTitleAndAccountId(title, accountId);
		//console.log("todo: ", existingTodo);
		if (!existingTodo) {
			const todo = await ToDo.create(req.body);
			//const todo = await findTodoByTitleAndAccountId(title, accountId);
			//console.log("todo: ", todo);
			res.json({
				"success": true,
				"data": todo,
			});
		} else {
			res.json({
				"success": false,
				"data": []
			});
		}

	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};

// PATCH Requests
export const updateToDo = async (req, res) => {
	try {
		if (!req.params.id) {
			res.json({
				"success": false,
				"data": { message: "cannot find id" }
			});
		}
		const searchId = req.params.id;
		const todo = await ToDo.update(req.body, {
			where: {
				id: searchId
			}
		});
		//const todo = await findTodoById(searchId);
		//console.log(`todo: ${todo}, id: ${req.params.id}`);
		if (todo) {
			res.json({
				"success": true,
				"data": todo
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};

// DELETE Request
export const deleteToDoByIdList = async (req, res) => {
	try {
		const ids = req.body.ids;
		if (!req.body.ids) {
			res.json({
				"success": false,
				"data": { message: "cannot find ids" }
			});
		}
		const count = await ToDo.destroy({
			where: {
				id: [...ids]
			}
		});
		if (count > 0) {
			res.json({
				"success": true,
				"data": { message: "To-Do gelöscht" }
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.message);
		res.json({
			"success": false,
			"data": { message: error.message }
		});
	}
};

// DELETE Request
export const deleteToDoByAccountId = async (req, res) => {
	try {
		if (!req.params.account_id) {
			res.json({
				"success": false,
				"data": { message: "cannot find account id" }
			});
		}
		const count = await ToDo.destroy({
			where: {
				account_id: req.params.account_id
			}
		});
		if (count > 0) {
			res.json({
				"success": true,
				"data": { message: "To-Do gelöscht" }
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.message);
		res.json({
			"success": false,
			"data": { message: error.message }
		});
	}
};

// DELETE Request
export const deleteToDo = async (req, res) => {
	try {
		if (!req.params.id) {
			res.json({
				"success": false,
				"data": { message: "cannot find id" }
			});
		}
		const count = await ToDo.destroy({
			where: {
				id: req.params.id
			}
		});
		if (count > 0) {
			res.json({
				"success": true,
				"data": { message: "To-Do gelöscht" }
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.message);
		res.json({
			"success": false,
			"data": { message: error.message }
		});
	}
};

// ---> Account <---
// GET Requests
export const getAllAccounts = async (_req, res) => {
	console.log("hier alle accounts");
	try {
		const accountList = await Account.findAll();
		if (accountList?.length > 0) {
			res.json({
				"success": true,
				"data": accountList
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};
// GET Requests
export const getAccountById = async (req, res) => {
	try {
		const account = await findAccountById(req.params.id);
		if (account) {
			res.json({
				"success": true,
				"data": account
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};
// GET Requests
export const getAccountByName = async (req, res) => {
	try {
		const account = await findAccountByName(req.params.name);
		//console.log(account);
		if (account) {
			res.json({
				"success": true,
				"data": account
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.log);
		res.json(badResponse);
	}
};
// POST Requests
export const createAccount = async (req, res) => {
	try {
		const name = req.body.name;
		const existingAccount = await findAccountByName(name);
		//console.log(existingAccount)
		if (!existingAccount) {
			const account = await Account.create(req.body);
			//console.log("neue Account: ", account)
			res.json({
				"success": true,
				"data": account
			});
		} else {
			res.json({
				"success": false,
				"data": { message: `${name} ist schon existiert` }
			});
		}

	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};

// PATCH Requests
export const updateAccount = async (req, res) => {
	try {
		if (!req.params.id) {
			res.json({
				"success": false,
				"data": { message: "cannot find id" }
			});
		}
		const account = await Account.update(req.body, {
			where: {
				id: req.params.id
			}
		});
		//const account = await findAccountById(req.params.id);
		if (account) {
			res.json({
				"success": true,
				"data": account
			});
			return;
		}
		res.json(badResponse);
	} catch (error) {
		console.error(error.message);
		res.json(badResponse);
	}
};
// DELETE Requests
export const deleteAccount = async (req, res) => {
	try {
		if (!req.params.id) {
			res.json({
				"success": false,
				"data": { message: "cannot find id" }
			});
		}
		const count = await Account.destroy({
			where: {
				id: req.params.id
			}
		});

		if (count > 0) {
			res.json({
				"success": true,
				"data": { message: "Account gelöscht" }
			});
			return;
		}
		res.json(badResponse);

	} catch (error) {
		console.error(error.message);
		res.json({
			"success": false,
			"data": { message: error.message }
		});
	}
};

export const checkPassword = async (req, res) => {
	try {
		const password = req.body.pw;
		const encrypted = req.body.encrypted;
		if (!password) {
			res.json(badResponse);
			return;
		}
		const compare = await comparePassword(password, encrypted);
		//console.log("compare: ", compare);
		if (compare) {
			console.log("pwd identisch!!!");
			res.json({ "success": true });
			return;
		}
		res.json({ "success": false });
	} catch (error) {
		console.error(error.message);
		res.json({ "success": false });
	}
};

// HELFER
const comparePassword = async (password, encrypted) => {
	const result = await bcrypt.compare(password, encrypted);
	if (result) {
		console.log(`Submitted password is correct!!! result: ${result}`);
		return true;
	}
	console.log(`Submitted password isn't correct!!! result: ${result}`);

	return false;
};

const findTodoById = async (searchId) => {
	console.log(`searching todo for id: ${searchId}`);
	try {
		const todo = await ToDo.findAll({
			where: {
				id: searchId
			}
		});

		if (todo?.length) {
			//console.log(`todo: ${todo[0]}, id: ${searchId}`);
			return todo[0];
		}
	} catch (error) {
		console.error(error);
	}
	return null;
};
const findTodoByTitle = async (searchTitle) => {
	try {
		const todo = await ToDo.findAll({
			where: {
				title: searchTitle
			}
		});
		if (todo?.length) {
			//console.log(`todo: ${todo[0]}, id: ${searchTitle}`);
			return todo[0];
		}
	} catch (error) {
		console.error(error);
	}
	return null;
};
const findAccountById = async (searchId) => {
	try {
		const account = await Account.findAll({
			where: {
				id: searchId
			}
		});
		if (account?.length) {
			//console.log(`account: ${account[0]}, id: ${searchId}`);
			return account[0];
		}

	} catch (error) {
		console.error(error);
		return null;
	}
};
const findAccountByName = async (searchName) => {
	//console.log("param: ", searchName)
	try {
		const account = await Account.findAll({
			where: {
				name: searchName
			}
		});
		//console.log(account)
		if (account?.length) {
			//console.log(`account: ${account[0]}, id: ${searchName}`);
			return account[0];
		}
	} catch (error) {
		console.error(error);
		return null;
	}
	return null;
};
const findTodoByTitleAndAccountId = async (searchTitle, accountId) => {
	try {
		const todo = await ToDo.findAll({
			where: {
				title: searchTitle,
				account_id: accountId
			}
		});
		if (todo?.length) {
			//console.log(`todo: ${todo[0]}, id: ${searchTitle}`);
			return todo[0];
		}
	} catch (error) {
		console.error(error);
	}
	return null;
};

