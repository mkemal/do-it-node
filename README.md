# Do-it!
JavaScript Grundkurs Projekt für die Weiterbildungsmaßnahme bei Alfatraining.

14.03.2022 bis 18.03.2022

## Authoren
+ Mustafa Kemal Öcel [GitHub](https://github.com/mkemaloecel)
+ Agron Kadriaj [GitHub](https://github.com/KKA11010) [GitLab](https://gitlab.com/FirstTerraner)

## Geschrieben mit Hilfe von:
+ Node.js Backend
+ MySQL (XAMPP Server)
+ React Frontend
+ HTML5/CSS

## Dieses Projekt bietet folgende Features:
+ Mehrere Benutzer und individuelle To-Do Listen in einer MySQL DB Erstellen
+ Für MySQL DB Verwalten eine Node.js Server Api Schreiben
    a. Account Daten erfassen (insert, update, delete, select usw.)
    b. ToDo Daten erfassen (insert, update, delete, select usw.)
    c. Models für die Account und ToDo Tabellen erstellen.
    d. Eine Api Router erstellen für außen zugriff.
+ Login/Logout (Simuliert mit Local Storage)
+ Benutzername validierung (Duplikate)
+ Passwort Verschlüsselung mit bcrypt
+ To-Do Liste Erfassen
+ Einträge erstellen
+ Einträge bearbeiten
+ Einträge sortieren
+ Einträge löschen
+ Mehrere erledigte Einträge löschen

## Installation
+ XAMPP wird benötigt: [XAMPP](https://www.apachefriends.org/de/download.html)
+ Git Projekt klonen:
```bash
git clone https://gitlab.com/mkemal/do-it-node.git
# change directory
cd do-it-node
```
+ XAMPP Server als Admin starten
+ Apache & MySQL starten und zu http://localhost/phpmyadmin navigieren.

+ Dort auf den "SQL" Reiter klicken und ein SQL Befehl durchführen.
+ DB User erstellen: 
  ```sql
  CREATE USER 'doit'@'%' IDENTIFIED VIA mysql_native_password USING 'test1234';GRANT ALL PRIVILEGES ON *.* TO 'doit'@'%' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;GRANT ALL PRIVILEGES ON `do-it`.* TO 'doit'@'%';
  ```
+ Backend und Frontend dependencies installieren:
```
# /do-it-node/server
npm i

# /do-it-node/frontend
npm i
```
+ Server & Frontend starten
```
# do-it-node/server
node server

# /do-it-node/frontend
npm run start
```

## Backend Bauen/Starten (Achtung: Bitte erst DB vorbereiten!!!)
```bash
cd /do-it-node/server
npm i
node server

```

## Frontend Bauen/Starten (Achtung: Bitte erst Server (Backend) starten!!!)
```bash
cd /do-it-node/frontend
npm i
npm run start

```

## Fehlermeldungen
Öffne ein "Issue" um den Fehler öffentlich zu besprechen oder implementiere eine Lösung und erstelle ein Pull-request.