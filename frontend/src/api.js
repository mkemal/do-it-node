import axios from 'axios'

// FETCH INIT
const baseReq = axios.create({ baseURL: 'http://127.0.0.1:3002/api', timeout: 3000 })

// FETCH HELPER
const fetchRoute = async (route, method, body) => {
	if (method === 'GET') {
		const resp = await baseReq.get(route)
		// console.log('resp in fetch: ', resp)
		if (!resp?.data || !resp?.data?.success) {
			return null
		}
		return resp.data
	}
	if (method === 'POST') {
		const resp = await baseReq.post(route, body)
		// console.log('resp in fetch: ', resp)
		return resp.data
	}
	if (method === 'PATCH') {
		const resp = await baseReq.patch(route, body)
		// console.log('resp in fetch: ', resp)
		if (!resp?.data || !resp?.data?.success) {
			return null
		}
		return resp.data
	}
	if (method === 'DELETE') {
		const resp = await baseReq.delete(route)
		// console.log('resp in fetch: ', resp)
		if (!resp?.data || !resp?.data?.success) {
			return null
		}
		return resp.data
	}
}

// GET TODOS
/* export const getAllTodos = async () => {
	const body = {
		orderby: [['id', 'DESC']]
	}
	const resp = await fetchRoute('/todos', 'POST', body)
	if (!resp) { return null }
	return resp.data
} */
export const getTodosByAccountId = async (accountId, orderby)=> {
	const body = {
		accountId,
		orderby: orderby || [['id', 'ASC']]
	}
	const resp = await fetchRoute('/todos-by-accountId', 'POST', body)
	if (!resp) { return null }
	return resp.data
}
/* export const getTodoById = async id => {
	const resp = await fetchRoute(`/todo/${id}`, 'GET')
	if (!resp) { return null }
	return resp.data
}
export const getTodoByTitle = async title => {
	const resp = await fetchRoute(`/todo-by-title/${title}`, 'GET')
	if (!resp) { return null }
	return resp.data
} */
// POST TODO
export const createTodo = async todo => {
	if (!todo) { return null }
	const body = {
		...todo,
		done: false
	}
	const resp = await fetchRoute('/todo/create', 'POST', body)
	if (!resp) { return null }
	return resp.data
}
// PATCH TODO
export const updateTodo = async (id, updatedTodo) => {
	const resp = await fetchRoute(`/todo/update/${id}`, 'PATCH', updatedTodo)
	if (!resp) { return null }
	return resp.data
}
// DELETE TODO
export const deleteTodo = async id => {
	const resp = await fetchRoute(`/todo/delete/${id}`, 'DELETE')
	if (!resp) { return null }
	return resp.data
}
export const deleteTodosByIds = async (account_id, ids) => {
	const body = {
		account_id,
		ids
	}
	const resp = await fetchRoute('/todo/deleteToDoByIdList', 'POST', body)
	if (!resp) { return null }
	return resp.data
}

// GET ACCOUNT
/* export const getAllAccounts = async () => {
	const resp = await fetchRoute('/accounts', 'GET')
	if (!resp) { return null }
	return resp.data
} */
/* export const getAccountById = async id => {
	const resp = await fetchRoute(`/account/${id}`, 'GET')
	if (!resp) { return null }
	return resp.data
} */
export const getAccountByName = async name => {
	const resp = await fetchRoute(`/account-by-name/${name}`, 'GET')
	if (!resp) { return null }
	return resp.data
}
// POST ACCOUNT
export const createAccount = async account => {
	if (!account || account.name.length < 1) { return null }
	const resp = await fetchRoute('/account/create', 'POST', account)
	if (!resp) { return null }
	return resp.data
}
// PATCH ACCOUNT
/* export const updateAccount = async (id, account) => {
	if (!account || account.name.length < 1) { return null }
	const resp = await fetchRoute(`/account/update/${id}`, 'PATCH', account)
	if (!resp) { return null }
	return resp.data
} */
// DELETE ACCOUNT
/* export const deleteAccount = async id => {
	if (!id) { return null }
	const resp = await fetchRoute(`/account/delete/${id}`, 'DELETE')
	if (!resp) { return null }
	return resp.data
} */
// COMPARE PWS
export const comparePws = async (pw, encrypted) => {
	if (!pw || !encrypted) { return null }
	const resp = await fetchRoute('/password/check', 'POST', { pw, encrypted })
	return resp?.success
}