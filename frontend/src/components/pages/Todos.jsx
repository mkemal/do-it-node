import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import Navigation from '../Navigation'
import './Todos.css'
import Welcome from '../Welcome'
import NewTodoModal from '../NewTodoModal'
import { deleteTodo, deleteTodosByIds, getTodosByAccountId, updateTodo } from '../../api'
import { formatMinutesToHrsMin, getTodosCount } from '../../helper'
import UpdateTodoModal from '../UpdateTodoModal'

const defaultModalState = { newTodo: false, updateTodo: false }
const defaultOverview = { all: 0, todo: 0, done: 0, total_est: 0 }

const TodosPage = ({ user, setUserCB }) => {

	// todo list
	const [todos, setTodos] = useState()
	// update todo list callback
	const updateTodos = useCallback(newTodos => { setTodos([...newTodos]) }, [setTodos])
	// todo overview numbers
	const [overview, setOverview] = useState(defaultOverview)
	// todo item to update in modal
	const [toUpdate, setToUpdate] = useState()
	// modal hidden state
	const [modal, setModal] = useState(defaultModalState)
	// close modal callback
	const closeModal = useCallback(() => { setModal(defaultModalState) }, [setModal])

	// todos functionalities
	const handleUpdateTodo = (e, todo) => {
		if (e?.target?.type === 'checkbox' || e?.target?.type === 'submit') {
			return
		}
		setToUpdate(todo)
		setModal({ ...modal, updateTodo: true })
	}
	const handleDelete = async todo => {
		const deleted = await deleteTodo(todo.id)
		if (deleted.message !== 'To-Do gelöscht') { return }
		todos.splice(todos.indexOf(todo), 1)
		updateTodos(todos)
	}
	const handleChangeDone = async todo => {
		const newTodo = { ...todo, done: !todo.done }
		const updated = await updateTodo(todo.id, newTodo)
		if (!updated) { return }
		todos[todos.indexOf(todo)] = newTodo
		updateTodos(todos)
	}
	const deleteAllDone = async () => {
		const doneIds = []
		todos.forEach(todo => todo.done && doneIds.push(todo.id))
		if (!doneIds.length) { return }
		const deleted = await deleteTodosByIds(user.id, doneIds)
		if (deleted.message !== 'To-Do gelöscht') { return }
		const newTodos = []
		todos.forEach(todo => !todo.done && newTodos.push(todo))
		updateTodos(newTodos)
	}
	const [sortCriteria, setSortCriteria] = useState('')
	const handleSort = async e => {
		if (sortCriteria === e.target.id) {
			const sorted = await getTodosByAccountId(user.id, [[e.target.id, 'DESC']])
			if (!sorted?.length) { return }
			setSortCriteria('')
			updateTodos(sorted)
			return
		}
		const sorted = await getTodosByAccountId(user.id, [[e.target.id, 'ASC']])
		if (!sorted?.length) { return }
		setSortCriteria(e.target.id)
		updateTodos(sorted)
	}

	// Get user todo list
	useEffect(() => {
		(async () => {
			if (user.isOn) {
				const userTodos = await getTodosByAccountId(user.id)
				setTodos(userTodos)
				return
			}
			setTodos(undefined)
		})()
	}, [user.isOn])

	// Update todo overview state
	useEffect(() => {
		if (!todos) { return }
		setOverview({
			all: todos.length,
			todo: getTodosCount(todos, false),
			done: getTodosCount(todos),
			total_est: formatMinutesToHrsMin(todos.map(todo => !todo.done && todo.est_time).reduce((a, b) => a + b, 0))
		})
	}, [todos])

	// Show Component for logged-out user
	if (!user.isOn) {
		return <>
			<main className='todosBody'>
				<Welcome />
			</main>
		</>
	}

	return (
		<>
			<Navigation username={user.name} setUserCB={setUserCB} />
			<main className='todosBody'>
				{user.isOn && todos && todos.length > 0 ?
					<>
						<div className='overviewWrap'>
							<h3 className='normalFontWeight'>
								{overview.all} Eintr{overview.all > 1 ? 'äge' : 'ag'}
							</h3>
							<h3 className='normalFontWeight'>
								{overview.todo} To-do{overview.todo > 1 ? '\'s' : ''}
							</h3>
							<h3 className='normalFontWeight'>
								{overview.done} Erledigt
							</h3>
							<h3 className='normalFontWeight'>
								Gesamtschätzung:&nbsp;{overview.total_est}
							</h3>
						</div>
						<div className="tableControls">
							<button className='myBtn greenBtn' onClick={() => setModal({ ...modal, newTodo: true })}>
								+ Neuer Eintrag
							</button>
							<button className="myBtn" onClick={deleteAllDone}>
								Alle erledigten Einträge löschen
							</button>
						</div>
						<table className="todoTable">
							<thead>
								<tr>
									<th id='title' onClick={handleSort}>To-Do</th>
									<th id='createdAt' onClick={handleSort}>Erstelldatum</th>
									<th id='due_date' onClick={handleSort}>Deadline</th>
									<th id='est_time' onClick={handleSort}>Zeit-Schätzung</th>
									<th id='done' onClick={handleSort}>Erledigt?</th>
									<th>Aktion</th>
								</tr>
							</thead>
							<tbody>
								{todos.map(todo =>
									<tr
										key={todo.id}
										onClick={e => handleUpdateTodo(e, todo)}
									>
										<td>{todo.title}</td>
										<td>{new Date(todo.createdAt).toLocaleDateString()}</td>
										<td>{new Date(todo.due_date).toLocaleDateString()}</td>
										<td>{formatMinutesToHrsMin(todo.est_time)}</td>
										<td>
											<input
												type="checkbox"
												checked={todo.done}
												onChange={() => handleChangeDone(todo)}
											/>
										</td>
										<td>
											<button onClick={() => handleDelete(todo)}>Löschen</button>
										</td>
									</tr>
								)}
							</tbody>
						</table>
					</>
					:
					<>
						<h2>Erstelle deinen Ersten Eintrag</h2>
						<button className='myBtn greenBtn' onClick={() => setModal({ ...modal, newTodo: true })}>
							+ Neuer Eintrag
						</button>
					</>
				}
			</main>
			{modal.newTodo &&
				<NewTodoModal
					userId={user.id}
					closeModal={closeModal}
					todos={todos}
					updateTodos={updateTodos}
				/>
			}
			{modal.updateTodo &&
				<UpdateTodoModal
					userId={user.id}
					closeModal={closeModal}
					todos={todos}
					updateTodos={updateTodos}
					todo={toUpdate}
				/>
			}
		</>
	)
}

export default TodosPage

TodosPage.propTypes = {
	user: PropTypes.object || undefined,
	setUserCB: PropTypes.func || undefined
}