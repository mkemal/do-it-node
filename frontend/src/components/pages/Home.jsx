import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './Home.css'
import Welcome from '../Welcome'

const Home = ({ user, setUserCB }) => {

	const handleLogout = () => {
		localStorage.removeItem('user')
		setUserCB({user: '', id: -1, isOn: false})
	}

	return (
		<>
			<main className="homeBody">
				{user.isOn ?
					<>
						<h1 className='appBigName'>Do-it!</h1>
						<h1 className='normalFontWeight'>Willkommen zurück, {user.name}</h1>
						<Link to="/todos">
							<button className='myBtn greenBtn'>
								Deine To-Do&apos;s ansehen
							</button>
						</Link>
						<p className='logoutP' onClick={handleLogout}>Abmelden</p>
					</>
					:
					<Welcome />
				}
			</main>
		</>
	)
}

export default Home

Home.propTypes = {
	user: PropTypes.object || undefined,
	setUserCB: PropTypes.func || undefined
}