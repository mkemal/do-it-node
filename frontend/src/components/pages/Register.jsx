import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { getAccountByName, createAccount } from '../../api'
import { passwordStrong, drawCanvas } from '../../helper'
import PropTypes from 'prop-types'
import './Register.css'

const Register = ({ setUserCB }) => {

	const navigate = useNavigate()

	const [input, setInput] = useState({ username: '', password: '' })

	const [noti, setNoti] = useState({ bad: false, txt: '' })

	const handleBad = (bad, txt) => {
		setNoti({ bad, txt })
		const t = setTimeout(() => {
			setNoti({ bad: false, txt: '' })
			clearTimeout(t)
		}, 2500)
	}

	const handleUsername = e => {
		if (!e.target.value) { return }
		setInput({ ...input, username: e.target.value })
	}

	const handlePassword = e => {
		const pwd = e.target.value
		if (!pwd) {
			drawCanvas(0, 'cvs')
			return
		}
		const strong = passwordStrong(pwd)
		const sicherheitszahl = +strong > 50 ? 50 : +strong
		drawCanvas(sicherheitszahl, 'cvs')
		setInput({ ...input, password: pwd })
	}

	const handleSubmit = async () => {
		const pattern = new RegExp('^[a-zA-Z][a-zA-Z0-9_-]*$')
		const isValid = (pattern.test(input.username))
		if (!isValid) {
			handleBad(true, 'Benutzername darf nur Buchstaben und Zahlen enthalten!')
			return
		}
		if (!input.username || !input.password) {
			handleBad(true, 'Benutzername oder Passwort leer!')
			return
		}
		const dbUser = await getAccountByName(input.username)
		if (dbUser) {
			handleBad(true, 'Benutzername existiert schon!!!')
			return
		}

		const user = await createAccount({ name: input.username, password: input.password, desc: 'normal user' })
		if (!user) {
			handleBad(true, 'Ups, da ist etwas schief gegangen. Versuche es später noch mal')
			return
		}
		console.log(user)
		localStorage.setItem('user', JSON.stringify({ name: user.username, id: user.id }))
		setUserCB({name: user.name, id: user.id, isOn: true})
		navigate('/todos')
	}

	return (
		<>
			<main className='registerBody'>
				<Link to="/">
					<h1 className='appBigName'>Do-it!</h1>
				</Link>
				<h1>Account erstellen</h1>
				<input
					type="text"
					placeholder="Nutzername"
					onChange={handleUsername}
					required
					className="myInput w300px"
				/>
				<input
					type="password"
					placeholder="Passwort"
					onChange={handlePassword}
					required
					className="myInput w300px"
				/>
				<canvas
					className="canvasWrap"
					id="cvs"
					width="200"
					height="25"
					hidden
				></canvas>
				<button
					className="myBtn greenBtn mt-05 w330px"
					onClick={handleSubmit}
				>
					Registrieren
				</button>
				<p>Bereits Nutzer? <Link to="/login">Hier einloggen.</Link></p>
				{noti.bad && <p className="badNotify">{noti.txt}</p>}
			</main>
		</>
	)
}

export default Register

Register.propTypes = {
	user: PropTypes.object || undefined,
	setUserCB: PropTypes.func || undefined
}