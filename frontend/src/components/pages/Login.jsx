import React, { useState } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { comparePws, getAccountByName } from '../../api'
import PropTypes from 'prop-types'
// import Navigation from '../Navigation'
import './Login.css'

const Login = ({ setUserCB }) => {

	const navigate = useNavigate()

	const [input, setInput] = useState({ username: '', password: '' })

	const [noti, setNoti] = useState({ bad: false, txt: '' })

	const handleNoti = (bad, txt) => {
		setNoti({ bad, txt })
		const t = setTimeout(() => {
			setNoti({ bad: false, txt: '' })
			clearTimeout(t)
		}, 2500)
	}

	const handleUsername = e => {
		if (!e.target.value) { return }
		setInput({ ...input, username: e.target.value })
	}

	const handlePassword = e => {
		if (!e.target.value) { return }
		setInput({ ...input, password: e.target.value })
	}

	const handleSubmit = async () => {
		if (!input.username || !input.password) {
			handleNoti(true, 'Benutzername oder Passwort leer!')
			return
		}
		const dbUser = await getAccountByName(input.username)
		if (!dbUser) {
			handleNoti(true, 'Der Benutzername ist nicht vorhanden!')
			return
		}
		const isValidUserInput = await checkUser(input.username, input.password, dbUser)
		if (!isValidUserInput) {
			handleNoti(true, 'Benutzername oder Passwort falsch!')
			return
		}
		localStorage.setItem('user', JSON.stringify({ name: dbUser.name, id: dbUser.id }))
		setUserCB({ name: dbUser.name, id: dbUser.id, isOn: true })
		void navigate('/todos')
	}

	return (
		<>
			{/* <Navigation showSignUp /> */}
			<main className='loginBody'>
				<Link to="/">
					<h1 className='appBigName'>Do-it!</h1>
				</Link>
				<h1>Login</h1>
				<input
					type="text"
					placeholder="Nutzername"
					onChange={handleUsername}
					required
					className="myInput w300px"
				/>
				<input
					type="password"
					placeholder="Passwort"
					onChange={handlePassword}
					required
					className="myInput w300px"
				/>
				<button
					className="myBtn greenBtn w330px mt-05"
					onClick={handleSubmit}
				>
					Anmelden
				</button>
				<p>Noch kein Account? <Link to="/register">Hier registrieren.</Link></p>
				{noti.bad && <p className="badNotify">{noti.txt}</p>}
			</main>
		</>
	)
}

export default Login

Login.propTypes = {
	user: PropTypes.object || undefined,
	setUserCB: PropTypes.func || undefined
}

const checkUser = (username, pw, user) => username === user.name && comparePws(pw, user.password)