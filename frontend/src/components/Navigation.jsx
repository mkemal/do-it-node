import React from 'react'
import PropTypes from 'prop-types'
import { Link, useLocation } from 'react-router-dom'
import './Navigation.css'

const Navigation = ({ username, showLogin, showSignUp, setUserCB }) => {

	const location = useLocation()

	const handleLogout = () => {
		localStorage.removeItem('user')
		setUserCB({name: '', id: -1, isOn: false})
	}

	return (
		<div className='navWrap'>
			<div className='flex'>
				<Link to='/'><h1 className='appName'>Do-it!</h1></Link>
				{username && username.length > 0 && location.pathname !== '/' &&
					<h1 className='appName'>{username}</h1>
				}
			</div>
			<div>
				{showLogin &&
					<Link to="/login">
						<button className='myBtn'>
							Einloggen
						</button>
					</Link>
				}
				{showSignUp &&
					<Link to="/register">
						<button className='myBtn'>
							Registrieren
						</button>
					</Link>
				}
				{username && username.length > 0 &&
					<button className='myBtn' onClick={handleLogout}>
						Abmelden
					</button>
				}
			</div>
		</div>
	)
}

export default Navigation

Navigation.propTypes = {
	username: PropTypes.string || undefined,
	showLogin: PropTypes.bool || undefined,
	showSignUp: PropTypes.bool || undefined,
	setUserCB: PropTypes.func || undefined
}