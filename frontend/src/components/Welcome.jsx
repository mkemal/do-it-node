import React from 'react'
import { Link } from 'react-router-dom'

const Welcome = () =>
	<>
		<Link to="/">
			<h1 className='appBigName'>Do-it!</h1>
		</Link>
		<h2 className='normalFontWeight'>
			Registriere dich oder logge dich ein...
		</h2>
		<p></p>{/* space */}
		<div className='flex'>
			<Link to="/login">
				<button className='myBtn'>
					Einloggen
				</button>
			</Link>
			&nbsp;&nbsp;&nbsp;&nbsp; {/* space */}
			<Link to="/register">
				<button className='myBtn'>
					Registrieren
				</button>
			</Link>
		</div>
	</>

export default Welcome