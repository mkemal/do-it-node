import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './NewTodoModal.css'
import { createTodo } from '../api'
import { calcEstTime, getToday, isValidDeadline, isValidEstTime } from '../helper'

const NewTodoModal = ({ userId, todos, updateTodos, closeModal }) => {

	const [error, setError] = useState({
		title: false,
		mins: false,
		deadline: false,
		fetch: false
	})
	const [input, setInput] = useState({
		title: undefined,
		hrs: undefined,
		mins: undefined,
		deadline: undefined
	})
	const handleTitleChange = e => {
		if (!e.target.value) { return }
		if (error.title) { setError({ ...error, title: false }) }
		setInput({ ...input, title: e.target.value })
	}
	const handleHrsChange = e => {
		if (!e.target.value) { return }
		setInput({ ...input, hrs: +e.target.value })
	}
	const handleMinsChange = e => {
		if (!e.target.value) { return }
		setError({ ...error, mins: false })
		setInput({ ...input, mins: +e.target.value })
	}
	const handleDateChange = e => {
		if (!e.target.value) { return }
		setError({ ...error, deadline: false })
		setInput({ ...input, deadline: new Date(e.target.value) })
	}
	const handleSubmit = async () => {
		if (error.fetch) { setError({ ...error, fetch: false }) }
		if (!input.title || !isValidDeadline(input.deadline, getToday()) || !isValidEstTime(input.hrs, input.mins)) {
			setError({
				title: input.title ? false : true,
				mins: !isValidEstTime(input.hrs, input.mins),
				deadline: !isValidDeadline(input.deadline, getToday())
			})
			return
		}
		let err = false
		todos.forEach(todo => {
			if (todo.title === input.title) {
				setError({ ...error, title: true, fetch: true })
				err = true
			}
		})
		if (!err) {
			const newTodo = await createTodo({
				account_id: userId,
				title: input.title,
				due_date: input.deadline,
				est_time: calcEstTime(input.hrs, input.mins)
			})
			todos.push(newTodo)
			updateTodos(todos)
		}
	}
	const handleModalClick = e => {
		if (e.target.className === 'modal') {
			closeModal()
		}
	}

	return (
		<div className="modal" onClick={handleModalClick}>
			<div className="modal-content">
				<div className="todoModalHeader">
					<h3 className="noMargin">Neuer Eintrag</h3>
				</div>
				<div className="todoModalBody">
					<div className="todoBodyContentWrap">
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="ToDo" required>To-do:</label>
							<input
								id="ToDo"
								type="text"
								className={`myInput${error.title ? ' badInput' : ''}`}
								placeholder="To-do Title"
								onChange={handleTitleChange}
							/>
						</div>
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="estimated" required>
								Zeit-Schätzung:
							</label>
							<div id="estimated" className="flex">
								<input
									type="number"
									className='myInput'
									placeholder="Stunden"
									onChange={handleHrsChange}
								/>
								&nbsp;&nbsp;
								<input
									type="number"
									className={`myInput${error.mins ? ' badInput' : ''}`}
									placeholder="Minuten"
									onChange={handleMinsChange}
								/>
							</div>
						</div>
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="dueDate" required>Deadline:</label>
							<input
								id="dueDate"
								type="date"
								className={`myInput${error.deadline ? ' badInput' : ''}`}
								onChange={handleDateChange}
							/>
						</div>
					</div>
				</div>
				<div className="todoModalFooter">
					<span className='badNotify'>
						{error.title && error.fetch && ' Eintrag existiert bereits! '}
						{error.deadline && ' Invalides Datum '}
						{error.mins && ' Invalide Zeit '}
					</span>
					<button className="myBtn greenBtn" onClick={handleSubmit}>Hinzufügen</button>
				</div>
			</div>
		</div>
	)
}

export default NewTodoModal

NewTodoModal.propTypes = {
	userId: PropTypes.number || undefined,
	todos: PropTypes.array || undefined,
	updateTodos: PropTypes.func || undefined,
	closeModal: PropTypes.func || undefined
}