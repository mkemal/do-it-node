import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './NewTodoModal.css'
import { updateTodo } from '../api'
import { calcEstTime, getHrsMinsFromMins, isValidDeadline, isValidEstTime } from '../helper'

const UpdateTodoModal = ({ userId, todos, updateTodos, closeModal, todo }) => {

	// input validation state
	const [error, setError] = useState({
		title: false,
		mins: false,
		deadline: false,
		fetch: false
	})
	// input values with default values of selected todo entry 
	const [input, setInput] = useState({
		title: todo.title,
		hrs: getHrsMinsFromMins(todo.est_time).hrs,
		mins: getHrsMinsFromMins(todo.est_time).mins,
		deadline: todo.due_date.split('T')[0]
	})
	// input handlers
	const handleTitleChange = e => {
		if (!e.target.value) { return }
		if (error.title) { setError({ ...error, title: false }) }
		setInput({ ...input, title: e.target.value })
	}
	const handleHrsChange = e => {
		if (!e.target.value) { return }
		setInput({ ...input, hrs: +e.target.value })
	}
	const handleMinsChange = e => {
		if (!e.target.value) { return }
		setError({ ...error, mins: false })
		setInput({ ...input, mins: +e.target.value })
	}
	const handleDateChange = e => {
		if (!e.target.value) { return }
		setError({ ...error, deadline: false })
		setInput({ ...input, deadline: e.target.value })
	}
	// update todo entry
	const handleSubmit = async () => {
		// guards
		console.log(input.deadline)
		if (error.fetch) { setError({ ...error, fetch: false }) }
		if (!input.title || !isValidDeadline(input.deadline, todo.createdAt) || !isValidEstTime(input.hrs, input.mins)) {
			setError({
				title: input.title ? false : true,
				mins: !isValidEstTime(input.hrs, input.mins),
				deadline: !isValidDeadline(input.deadline, todo.createdAt)
			})
			return
		}
		// update
		const newTodo = await updateTodo(todo.id, {
			account_id: userId,
			title: input.title,
			due_date: input.deadline,
			est_time: calcEstTime(input.hrs, input.mins)
		})
		// guard
		if (!newTodo) { setError({ ...error, fetch: true }) }
		// update todo entry of todo list state 
		todos[todos.indexOf(todo)] = {
			...todo,
			title: input.title,
			due_date: input.deadline,
			est_time: calcEstTime(input.hrs, input.mins)
		}
		// update todo list state
		updateTodos(todos)
		closeModal()
	}
	// close modal
	const handleModalClick = e => {
		if (e.target.className === 'modal') {
			closeModal()
		}
	}

	return (
		<div className="modal" onClick={handleModalClick}>
			<div className="modal-content">
				<div className="todoModalHeader">
					<h3 className="noMargin">Eintrag bearbeiten</h3>
				</div>
				<div className="todoModalBody">
					<div className="todoBodyContentWrap">
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="ToDo" required>To-do:</label>
							<input
								id="ToDo"
								type="text"
								className={`myInput${error.title ? ' badInput' : ''}`}
								placeholder="To-do Title"
								defaultValue={input.title || ''}
								onChange={handleTitleChange}
							/>
						</div>
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="estimated" required>
								Zeit-Schätzung:
							</label>
							<div id="estimated" className="flex">
								<input
									type="number"
									className='myInput'
									placeholder="Stunden"
									defaultValue={input.hrs || ''}
									onChange={handleHrsChange}
								/>
								&nbsp;&nbsp;
								<input
									type="number"
									className={`myInput${error.mins ? ' badInput' : ''}`}
									placeholder="Minuten"
									defaultValue={input.mins || ''}
									onChange={handleMinsChange}
								/>
							</div>
						</div>
						<div className="inputWrap">
							<label className="inputLabel" htmlFor="dueDate" required>Deadline:</label>
							<input
								id="dueDate"
								type="date"
								className={`myInput${error.deadline ? ' badInput' : ''}`}
								defaultValue={input.deadline || new Date().toLocaleDateString()}
								onChange={handleDateChange}
							/>
						</div>
					</div>
				</div>
				<div className="todoModalFooter">
					<span className='badNotify'>
						{error.deadline && ' Invalides Datum '}
						{error.mins && ' Invalide Zeit '}
					</span>
					<button className="myBtn greenBtn" onClick={handleSubmit}>Speichern</button>
				</div>
			</div>
		</div>
	)
}

export default UpdateTodoModal

UpdateTodoModal.propTypes = {
	userId: PropTypes.number || undefined,
	todos: PropTypes.array || undefined,
	updateTodos: PropTypes.func || undefined,
	closeModal: PropTypes.func || undefined,
	todo: PropTypes.object || undefined
}