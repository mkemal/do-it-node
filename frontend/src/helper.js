export const formatEstimatedTime = (hrs, min) => {
	if (!isNumber(hrs)) { return hrs }
	if (hrs == 0) { return `${min}min.` }
	if (min == 0) { return `${hrs}std.` }
	return `${hrs}std : ${min}min`
}

export const formatMinutesToHrsMin = minuten => formatEstimatedTime(Math.floor(minuten / 60), minuten % 60)

export const getHrsMinsFromMins = minuten => ({ hrs: Math.floor(minuten / 60), mins: minuten % 60 })

export const getTodosCount = (todosArr, done = true) => {
	const todos = []
	todosArr.forEach(todo => done ? todo.done && todos.push(todo) : !todo.done && todos.push(todo))
	return todos.length
}

export const calcEstTime = (hrs, mins) => {
	if (!hrs && !mins) { return 0 }
	if (!hrs) { return mins }
	if (!mins) { return hrs * 60 }
	return hrs * 60 + mins
}

export const isValidDeadline = (dueDate, createdDate) => {
	const oneDayinMs = 86400000
	const dueMs = new Date(dueDate).getTime()
	const createdMs = new Date(createdDate).getTime()
	return (createdMs - dueMs) < oneDayinMs
}

export const getToday = () => `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`

export const isValidEstTime = (hrs, mins) =>
	(hrs && !mins && hrs >= 0) || (!hrs && mins && mins >= 0) || (hrs && hrs >= 0 && mins && mins >= 0)

// CANVAS C&P
export const passwordStrong = password => getPasswordStrong(password)

export const drawCanvas = (percentage, canvasid) => {
	percentage *= 2
	let msg = ""
	var error = !isNumber(percentage)
	if (!error && (percentage < 0 || percentage > 100)) {
		error = true
	}
	percentage = percentage.toString().replace(/[^0-9]/g, '')
	var canvas = document.querySelector('#' + canvasid)
	canvas.hidden = false
	canvas.clear = true
	canvas = document.getElementById('cvs')
	if (canvas.getContext) {
		var context = canvas.getContext('2d')
		context.clearRect(0, 0, canvas.width, canvas.height)
		context.fillStyle = "rgb(143, 143, 143)"
		context.fillRect(0, 0, canvas.width, canvas.height)
		var percWidth = Math.round(canvas.width / 100)
		if (!error) {
			for (var i = 0; i <= percentage; i++) {
				var r, g, b
				if (i <= 50) {
					r = 255
					g = Math.round((255 * i) / 50)
					b = 0
				}
				else {
					r = Math.round((255 * (100 - i)) / 50)
					g = 255
					b = 0
				}
				context.fillStyle = "rgb(" + r + ", " + g + ", " + b + ")"
				context.fillRect(Math.round(i * canvas.width / 100) - percWidth, 0, percWidth, canvas.height)
			}
			context.font = "bold 30px sans-serif"
			context.fillStyle = "rgb(255,255,255)"
			context.textBaseline = "middle"
			msg = percentage + "%"
		}
		else {
			context.fillStyle = "rgb(255,0,0)"
			context.fillRect(0, 0, canvas.width, canvas.height)
			context.font = "bold 30px sans-serif"
			context.fillStyle = "rgb(255,255,255)"
			context.textBaseline = "middle"
			msg = "ERROR: Wrong input"
		}
		context.fillStyle = "rgb(60,60,60)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2) + 2, canvas.height / 2 + 2)
		context.fillStyle = "rgb(90,90,90)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2) + 1, canvas.height / 2 + 1)
		context.fillStyle = "rgb(255,255,255)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2), canvas.height / 2)
		context.strokeText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2), canvas.height / 2)
		context.globalAlpha = 0.4
		context.fillStyle = "rgb(255, 255, 255)"
		context.fillRect(0, 0, canvas.width, (canvas.height / 32) * 15)
		context.globalAlpha = 1.0
		context.fillStyle = "rgb(0, 0, 0)"
		roundRect(context, 0, 0, canvas.width, canvas.height, 10)
	}
}
const roundRect = (ctx, x, y, width, height, radius, fill, stroke) => {
	if (typeof stroke == "undefined") {
		stroke = true
	}
	if (typeof radius === "undefined") {
		radius = 5
	}
	//edge clearing
	var gdest = ctx.globalCompositeOperation
	var fstyle = ctx.fillStyle
	ctx.globalCompositeOperation = "destination-out"
	ctx.fillStyle = "rgba(0,0,0,1.0)"
	ctx.beginPath()
	ctx.moveTo(x, y + radius)
	ctx.quadraticCurveTo(x, y, x + radius, y)
	ctx.lineTo(x, y)
	ctx.lineTo(x, y + radius)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x, y + height - radius)
	ctx.quadraticCurveTo(x, y + height, x + radius, y + height)
	ctx.lineTo(x, y + height)
	ctx.lineTo(x, y + height - radius)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x + width - radius, y)
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius)
	ctx.lineTo(x + width, y)
	ctx.lineTo(x + width - radius, y)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x + width, y + height - radius)
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
	ctx.lineTo(x + width, y + height)
	ctx.lineTo(x + width, y + height - radius)
	ctx.closePath()
	ctx.fill()
	ctx.fillStyle = fstyle
	ctx.globalCompositeOperation = gdest
	ctx.beginPath()
	ctx.moveTo(x + radius, y)
	ctx.lineTo(x + width - radius, y)
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius)
	ctx.lineTo(x + width, y + height - radius)
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
	ctx.lineTo(x + radius, y + height)
	ctx.quadraticCurveTo(x, y + height, x, y + height - radius)
	ctx.lineTo(x, y + radius)
	ctx.quadraticCurveTo(x, y, x + radius, y)
	ctx.closePath()
	if (stroke) {
		ctx.stroke()
	}
	if (fill) {
		ctx.fill()
	}
}
const getPasswordStrong = (password) => {
	const regExArray = [
		/[a-z]/, /[A-Z]/, /[0-9]/, /[^A-Za-z0-9]/
	]
	let strong = 0
	//console.log(password);
	regExArray.forEach(regEx => {
		const myRegex = new RegExp(regEx)
		const check = myRegex.test(password)
		//console.log(`check regex: ${regEx}, gefunden? ${check}`);
		if (check) {
			//console.log(`gefundene regex: ${regEx}`);
			strong += 5
		}
	})

	let counter = 0
	for (let i = 100; i >= 8; i -= 5) {
		if (password.length > i) {
			strong += 2

		}
		if (i < 11) {
			counter++
			for (let j = 10; j > 0; j--) {
				strong += 1
				//console.log(counter, i, j);
			}
		}

	}
	//console.log(strong);
	return strong
}
const isNumber = (n) => !isNaN(parseFloat(n)) && isFinite(n)