import './App.css'
import { Routes, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import TodosPage from './components/pages/Todos'
import React, { useState, useCallback, useEffect } from 'react'
import Login from './components/pages/Login'
import Register from './components/pages/Register'

const defaultState = {
	name: '',
	id: -1,
	isOn: false
}

const App = () => {

	const [user, setUser] = useState(defaultState)

	const setUserCB = useCallback(newState => { setUser(newState) }, [setUser])

	useEffect(() => {
		const userString = localStorage.getItem('user')
		if (!userString?.length) { return }
		const user = JSON.parse(userString)
		if (user.name) {
			setUser({ name: user.name, id: user.id, isOn: true })
			return
		}
		setUser(defaultState)
	}, [])

	return (
		<div className="App">
			<Routes>
				<Route
					path="/"
					element={<Home user={user} setUserCB={setUserCB} />}
				/>
				<Route
					path="/todos"
					element={<TodosPage user={user} setUserCB={setUserCB} />}
				/>
				<Route
					path="/register"
					element={<Register user={user} setUserCB={setUserCB} />}
				/>
				<Route
					path="/login"
					element={<Login user={user} setUserCB={setUserCB} />}
				/>
			</Routes>
		</div>
	)
}

export default App
